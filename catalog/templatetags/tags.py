from django import template
from catalog.models import Category

register = template.Library()

@register.inclusion_tag('catalog/categories_tag.html')
def categories():
	nodes = Category.objects.all()
	return {'nodes': nodes}