from django.db import models
from django.urls import reverse
from mptt.models import MPTTModel, TreeForeignKey

class Category(MPTTModel):
    name = models.CharField(
            max_length=64, 
            unique=True
        )
    parent = TreeForeignKey(
            'self', 
            null=True, 
            blank=True, 
            related_name='children', 
            db_index=True,
            on_delete=models.CASCADE
        )
    slug = models.SlugField(
            max_length=128, 
            db_index=True, 
            unique=True
        )

    def get_absolute_url(self):
        return reverse("product-list-by-category", 
                    kwargs={"category": self.slug})

    class Meta:
        verbose_name_plural = 'Categories'    

    class MPTTMeta:
        order_insertion_by = ['name']

    def __str__(self):
        return self.name

class ProductItem(models.Model):
    name = models.CharField(
            max_length=64,
            unique=True,
            db_index=True,
            verbose_name='Name'
        )
    category = models.ForeignKey(
            Category,
            null=True,
            blank=True,
            on_delete=models.CASCADE
    )
    slug = models.SlugField(
            max_length=128,
            unique=True,
            db_index=True,
            verbose_name='Slug'
        )
    price = models.DecimalField(
            max_digits=6,
            decimal_places=2,
            verbose_name='Price',
            default=0
        )
    image = models.ImageField(
            upload_to='products/',
            null=True,
            blank=True,
            verbose_name='Image'
        )
    description = models.TextField(
            null=True,
            verbose_name='Description',
            blank=True
        )
    vendor_link = models.CharField(
            max_length=256,
            null=True,
            blank=True,
            verbose_name='Vendor link'
        )

    def __str__(self):
        return self.name

    def get_price(self):
        return 'RUB%s' % self.price
    
    def get_absolute_url(self):
        return reverse("product-detail", 
                    kwargs={"slug": self.slug})
    
    class Meta:
        ordering = ['name']
        verbose_name = 'Product'
        verbose_name_plural = 'Products'
        index_together = [
            ['id', 'slug']
        ]

