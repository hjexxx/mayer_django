from django.contrib import admin
from mptt.admin import DraggableMPTTAdmin
from catalog.models import ProductItem, Category

@admin.register(ProductItem)
class ProductItemAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('name', )}
    search_fields = ['name']

admin.site.register(
        Category, DraggableMPTTAdmin, 
        prepopulated_fields = {'slug': ('name', )})