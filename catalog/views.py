from django.shortcuts import render, get_object_or_404
from django.views.generic import ListView, DetailView
from catalog.models import ProductItem, Category

class ProductList(ListView):
    template_name = 'catalog/productList.html'

    def get_queryset(self, **kwargs):
        _category_slug = self.kwargs['category']
        if _category_slug != 'catalog':
            self.category = get_object_or_404(
                    Category, 
                    slug=_category_slug
                )     
            return ProductItem.objects.filter(
                    category=self.category)
        else:
            q = self.request.GET.get('q', None)
            print(q)
            if not q:
                return ProductItem.objects.all()
            else:
                return ProductItem.objects.filter(
                        name__icontains=q)
        
class ProductDetail(DetailView):
    model = ProductItem
    template_name = 'catalog/productDetail.html'
    
    