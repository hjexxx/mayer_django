from django.urls import path
from catalog.views import ProductList, ProductDetail
from django.views.generic.base import RedirectView

urlpatterns = [
    path('product/<slug>', 
            ProductDetail.as_view(), 
            name='product-detail'),

    path('<category>/', ProductList.as_view(), 
            name='product-list-by-category'),

    path('catalog/', ProductList.as_view(), 
            name='product-list'),
            
    path('', RedirectView.as_view(url='catalog', 
            permanent=False), name='index')
]
