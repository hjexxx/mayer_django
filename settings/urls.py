from django.conf.urls import url, include
from django.conf.urls.static import static
from django.contrib import admin
from django.conf import settings
import catalog.urls

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'', include('catalog.urls'))
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
